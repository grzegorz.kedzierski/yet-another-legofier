package yet.another.legofier

import org.junit.Test
import yet.another.legofier.bricks.BrickSets
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

class AppTest {

    @Test
    fun readFile() {
        val imageService = ImageService()


        val genericFlats1x1: Set<Brick> = Bricks.generic_flats_1x1

        ImageIO.read(File("/Users/g.kedzierski/Downloads/gked.jpeg"))
            .let {
                imageService.pixelate(
                    it,
                    colorReducer = { rgb -> rgb.averageSqrRGB() },
                    colorMapper = Bricks.findBestBrickBasedOnColor(Bricks.generic_flats_1x1
//                        .filterNot { b -> b.rgb == BrickSets.GenericColors.COFFEE.rgb }
//                        .filterNot { b -> b.rgb == BrickSets.GenericColors.WINE.rgb }
//                        .filterNot { b -> b.rgb == BrickSets.GenericColors.ROSY_BROWN.rgb }
                        .toSet()
                    ),
                    legoPainter = BufferedImage::legoBrickPainter
                )
            }
            .let { ImageIO.write(it, "png", File("/Users/g.kedzierski/Downloads/gked_2.png")) }
    }
}