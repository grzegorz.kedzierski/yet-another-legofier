package yet.another.legofier;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RGBTest {
    @Test
    public void should_create_good_rbg_values() {
        int color = 0xE1FF10;
        RGB rgb = RGBExtKt.toRGB(color, null);
        assertThat(rgb.getRed(), equalTo(0xE1));
        assertThat(rgb.getGreen(), equalTo(0xFF));
        assertThat(rgb.getBlue(), equalTo(0x10));
    }

    @Test
    public void should_create_good_rbg_values_for_edge_input_values() {
        int color = 0xFFAA00;
        RGB rgb = RGBExtKt.toRGB(color, null);
        assertThat(rgb.getRed(), equalTo(0xFF));
        assertThat(rgb.getGreen(), equalTo(0xAA));
        assertThat(rgb.getBlue(), equalTo(0x00));
    }

    @Test
    public void should_create_negative_color() {
        int color = 0xFFAA00;
        RGB rgb = RGBExtKt.toRGB(color, null).negative();
        assertThat(rgb.getRed(), equalTo(0x00));
        assertThat(rgb.getGreen(), equalTo(0x55));
        assertThat(rgb.getBlue(), equalTo(0xFF));
    }

    @Test
    public void should_return_proper_int_rgb() {
        int color = 0xFFAA00;
        int colorFromRgb = RGBExtKt.toRGB(color, null).toInt();
        assertThat(color, equalTo(colorFromRgb));
    }

}