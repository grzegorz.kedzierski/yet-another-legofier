package yet.another.legofier

import yet.another.legofier.bricks.BrickSets

data class Brick(val rgb: RGB, val id: String, val width: Int)

object Bricks {
    val findBestBrickBasedOnColor: (Set<Brick>) -> (RGB) -> RGB =
        { bricks ->
            { rgb ->
                bricks.map { it.rgb }.reduce { a, b ->
                    if (rgb.distanceEuclidean(a) < rgb.distanceEuclidean(b)) a else b
                }
            }
        }

    val lego_flats_1x1 = BrickSets.Lego.flat_tiles_1x1
    val generic_flats_1x1 = BrickSets.Generic.flat_tiles_1x1
}