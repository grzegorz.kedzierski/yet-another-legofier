package yet.another.legofier

import java.awt.Color
import kotlin.math.pow
import kotlin.math.sqrt

data class RGB(val red: Int, val green: Int, val blue: Int, val name: String? = null) {

    fun toInt(): Int = (red shl 16) + (green shl 8) + blue

    fun toColor(): Color = Color(red, green, blue)

    fun negativeRed(): RGB = this.copy(red = negativeColor(red))
    fun negativeGreen(): RGB = this.copy(green = negativeColor(green))
    fun negativeBlue(): RGB = this.copy(blue = negativeColor(blue))

    fun lighter(by: Int = 0x0A): RGB =
        this.copy(
            red = (red + by).let { if (it > 0xFF) 0xFF else it },
            green = (green + by).let { if (it > 0xFF) 0xFF else it },
            blue = (blue + by).let { if (it > 0xFF) 0xFF else it })

    fun darken(by: Int = 0x0A): RGB =
        this.copy(
            red = (red - by).let { if (it < 0) 0 else it },
            green = (green - by).let { if (it < 0) 0 else it },
            blue = (blue - by).let { if (it < 0) 0 else it })

    fun negative(): RGB = RGB(red = negativeColor(red), green = negativeColor(green), blue = negativeColor(blue))

    fun distanceEuclidean(otherColor: RGB) =
        sqrt(
            (red - otherColor.red).toDouble().pow(2)
                    + (green - otherColor.green).toDouble().pow(2)
                    + (blue - otherColor.blue).toDouble().pow(2)
        )

    fun distanceRedmean(otherColor: RGB) =
        ((red + otherColor.red) / 2).let { r ->
            sqrt(
                (2 + r / 256) * ((red - otherColor.red).toDouble().pow(2))
                        + 4 * ((green - otherColor.green).toDouble().pow(2))
                        + (2 + (255 - r) / 256) * ((blue - otherColor.blue).toDouble().pow(2))
            )
        }

    private fun negativeColor(color: Int) = 0xFF - color
}