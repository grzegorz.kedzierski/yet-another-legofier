package yet.another.legofier

import java.awt.image.BufferedImage

fun BufferedImage.legoSolidPixelByPixelLegoPainter(x0: Int, y0: Int, pixelsPerBrickEdge: Int, color: RGB) {
    for (x in x0 until x0 + pixelsPerBrickEdge) {
        for (y in y0 until y0 + pixelsPerBrickEdge) {
            this.setRGB(x, y, color.toInt())
        }
    }
}

fun BufferedImage.legoBrickPainter(x0: Int, y0: Int, pixelsPerBrickEdge: Int, color: RGB) {
    BufferedImage(pixelsPerBrickEdge, pixelsPerBrickEdge, BufferedImage.TYPE_INT_RGB).apply {
        createGraphics().apply {
            setColor(color.toColor())
            fill3DRect(0, 0, pixelsPerBrickEdge, pixelsPerBrickEdge, true)
            dispose()
        }
    }.let {
        this.createGraphics().apply {
            drawImage(it, x0, y0, pixelsPerBrickEdge, pixelsPerBrickEdge, null)
            dispose()
        }
    }
}