package yet.another.legofier


import mu.KotlinLogging
import java.awt.image.BufferedImage

class ImageService {
    private val logger = KotlinLogging.logger {}

    fun pixelate(
        imgSource: BufferedImage,
        noBricksHorizontal: Int = 64,
        colorReducer: (List<RGB>) -> RGB = { it.averageRGB() },
        colorMapper: (RGB) -> RGB = { it },
        legoPainter: BufferedImage.(Int, Int, Int, RGB) -> Unit = BufferedImage::legoBrickPainter
    ): BufferedImage {
        val width = imgSource.width - (imgSource.width % noBricksHorizontal)
        val pixelsPerBrickEdge = width / noBricksHorizontal
        val height = imgSource.height - (imgSource.height % pixelsPerBrickEdge)

        logger.info {
            "Original dimension: ${imgSource.width}x${imgSource.height}, new dimension: ${width}x$height, pixels per brick edge: $pixelsPerBrickEdge"
        }

        return (0..(width - pixelsPerBrickEdge) step pixelsPerBrickEdge).map { i ->
            (0..(height - pixelsPerBrickEdge) step pixelsPerBrickEdge).map { j ->
                (i until i + pixelsPerBrickEdge - 1)
                    .flatMap { x ->
                        (j until j + pixelsPerBrickEdge - 1).map { y -> imgSource.getRGB(x, y).toRGB().negativeRed().negativeBlue() }
                    }
                    .let { colorReducer(it) }
                    .let { colorMapper(it) }
            }
        }.renderLegoImage(width, height, pixelsPerBrickEdge, legoPainter)
    }
}
