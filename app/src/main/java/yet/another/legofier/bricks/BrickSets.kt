package yet.another.legofier.bricks

import yet.another.legofier.Brick
import yet.another.legofier.RGB
import yet.another.legofier.toRGB

/*
  https://www.bartneck.de/2016/09/09/the-curious-case-of-lego-colors/
  https://rebrickable.com/colors/
*/
object BrickSets {

    // from https://www.colorhexa.com/
    enum class GenericColors(val rgb: RGB) {
        POWDER_BLUE(0xb0e0e6.toRGB("Powder Blue")),
        LIGHT_BLUE(0xadd8e6.toRGB("Light Blue")),
        BLUE(0x0000ff.toRGB("Blue")),
        LIGHT_GREEN(0x90ee90.toRGB("Light Green")),
        GREEN(0x008000.toRGB("Green")),
        DARK_GREEN(0x013220.toRGB("Dark Green")),
        LIGHT_YELLOW(0xffffed.toRGB("Light Yellow")),
        YELLOW(0xffff00.toRGB("Yellow")),
        BEIGE(0xf5f5dc.toRGB("Beige")),
        KHAKI(0xf0e68c.toRGB("Khaki")),
        ORANGE(0xffa500.toRGB("Orange")),
        RED(0xff0000.toRGB("Red")),
        ROSY_BROWN(0xbc8f8f.toRGB("Rosy Brown")),
        WINE(0x722f37.toRGB("Wine")),
        WHITE(0xffffff.toRGB("White")),
        LIGHT_GRAY(0xd3d3d3.toRGB("Light gray")),
        DARK_GRAY(0xa9a9a9.toRGB("Dark Gray")),
        BLACK(0x000000.toRGB("Black")),
        COFFEE(0x6f4e37.toRGB("Coffee")),
        PURPLE(0x800080.toRGB("Purple")),
        LIGHT_PINK(0xffb6c1.toRGB("Light Pink")),
        MEDIUM_PURPLE(0x9370db.toRGB("Medium Purple")),
    }

    object Generic {
        val flat_tiles_1x1 = GenericColors.values().map { Brick(it.rgb, it.name, width = 1) }.toSet()
    }

    object Lego {
        val flat_tiles_1x1 = setOf(
            Brick(0xEF3340.toRGB(), "307021/3070", 1),
            Brick(0xFFCD00.toRGB(), "307024/3070", 1),
            Brick(0xD9D9D6.toRGB(), "307001/3070", 1),
            Brick(0x27251F.toRGB(), "307026/3070", 1),
            Brick(0x003DA5.toRGB(), "4206330/3070", 1),
            Brick(0xD9C89E.toRGB(), "4125253/3070", 1),
            Brick(0x00843D.toRGB(), "4558593/3070", 1),
            Brick(0x7A3E3A.toRGB(), "4211288/3070", 1),
            Brick(0x5B6770.toRGB(), "4210848/3070", 1),
            Brick(0xA2AAAD.toRGB(), "4211415/3070", 1),
            Brick(0xB5BD00.toRGB(), "4537251/3070", 1),
            Brick(0x720E0F.toRGB(), "4550169/3070", 1),
            Brick(0xFF8200.toRGB(), "4558595/3070", 1),
            Brick(0x003865.toRGB(), "4631385/3070", 1),
            Brick(0x71C5E8.toRGB(), "4655243/3070", 1),
            Brick(0x2C5234.toRGB(), "6055171/3070", 1),
            Brick(0x9B945F.toRGB(), "6055172/3070", 1),
            Brick(0xFFA300.toRGB(), "6065504/3070", 1),
            Brick(0xAF1685.toRGB(), "6099364/3070", 1),
            Brick(0xE93CAC.toRGB(), "6133726/3070", 1),
            Brick(0x009639.toRGB(), "6172375/3070", 1),
            Brick(0x5B7F95.toRGB(), "6138232/3070", 1),
            Brick(0xCC702A.toRGB(), "6177146/3070", 1),
            Brick(0x008675.toRGB(), "6213782/3070", 1),
            Brick(0x789F90.toRGB(), "6223913/3070", 1),
            Brick(0x9CDBD9.toRGB(), "6251846/3070", 1),
            Brick(0xF1A7DC.toRGB(), "6251940/3070", 1)
        )
    }
}