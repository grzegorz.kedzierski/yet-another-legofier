package yet.another.legofier

import mu.KotlinLogging
import java.awt.image.BufferedImage
import kotlin.math.sqrt

fun List<RGB>.averageSqrRGB(): RGB {
    val size = this.count()

    val averageRed = sqrt(this.sumOf { it.red * it.red } / size.toFloat()).toInt()
    val averageGreen = sqrt(this.sumOf { it.green * it.green } / size.toFloat()).toInt()
    val averageBlue = sqrt(this.sumOf { it.blue * it.blue } / size.toFloat()).toInt()

    return RGB(averageRed, averageGreen, averageBlue)
}

fun List<RGB>.averageRGB(): RGB {
    val size = this.count()

    val averageRed = this.sumOf { it.red } / size
    val averageGreen = this.sumOf { it.green } / size
    val averageBlue = this.sumOf { it.blue } / size

    return RGB(averageRed, averageGreen, averageBlue)
}

fun Int.toRGB(name: String? = null): RGB {
    val red = (this and 0xFF0000) shr 16
    val green = (this and 0xFF00) shr 8
    val blue = this and 0xFF
    return RGB(red, green, blue, name)
}

fun List<List<RGB>>.renderLegoImage(
    width: Int,
    height: Int,
    pixelsPerBrickEdge: Int,
    legoPainter: BufferedImage.(Int, Int, Int, RGB) -> Unit
): BufferedImage =
    KotlinLogging.logger {}
        .info {
            "Count: ${this.flatten().map { it.name }.groupingBy { it }.eachCount()}"
        }
        .let { _ ->
            BufferedImage(width, height, BufferedImage.TYPE_INT_RGB).also { img ->
                this.forEachIndexed { i, row ->
                    row.forEachIndexed { j, brick ->
                        img.legoPainter(i * pixelsPerBrickEdge, j * pixelsPerBrickEdge, pixelsPerBrickEdge, brick)
                    }
                }
            }
        }